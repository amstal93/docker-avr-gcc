# docker-avr-gcc

This repository sets up a simple Docker image, based on Ubuntu 18.04 and the official `avr-gcc` tarball from Atmel/Microchip. 


## Usage

This image is intended to be used for AVR8 automated builds. An appropriate GitLab CI configuration will be needed for the relevant project, for example something like:

```
image: atinywatchdog/avr-gcc:3.6.2.1759

variables:
  GIT_SUBMODULE_STRATEGY: recursive

build:
  stage: build
  script:
  - make all
  artifacts:
    name: "main_$CI_COMMIT_REF_NAME"
    paths:
    - main.hex
```

This assumes that there is an existing Makefile which actually performs the build process. 


## Building this image

The release name of the toolchain is passed into Docker as an argument, like so:

```
export AVR_GCC_VER=3.6.2.1759
docker build --pull --build-arg AVR_GCC_VER=$AVR_GCC_VER -t atinywatchdog/avr-gcc:$AVR_GCC_VER .
```

This will create a Docker image tagged with the toolchain version. 

_**NOTE:** The URL for fetching the toolchain tarball may be changed by Microchip without notice, which will mean the Dockerfile needs to be updated with the new URL._


## Uploading this image

The built image can then be pushed up to Docker Hub, provided access to the "atinywatchdog" account is available. 

First, login with:

```
$ docker login -u <USER> -p <PASSWORD>
```

Then push the image with:

```
$ docker push atinywatchdog/avr-gcc:$AVR_GCC_VER
```
